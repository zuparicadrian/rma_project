//
//  AppCoordinator.swift
//  EventsApp
//
//  Created by Adrian Zuparic on 7/12/20.
//

import UIKit

protocol Coordinator: AnyObject {
    var childCoordinators: [Coordinator] { get }
    func start()
    func childDidFinish(_ childCoordinator: Coordinator)
}

extension Coordinator {
    func childDidFinish(_ childCoordinator: Coordinator) {}
}


final class AppCoordinator {
    private(set) var childCoordinators: [Coordinator] = []
    private let window: UIWindow

    init(window: UIWindow) {
        self.window = window
    }

    func start() {
        let navigationController = UINavigationController()
        navigationController.navigationBar.tintColor = .primary
        let eventListCoordinator = EventListCoordinator(navigationController: navigationController)
        childCoordinators.append(eventListCoordinator)
        eventListCoordinator.start()

        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
}
