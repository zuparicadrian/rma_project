//
//  EventDetailViewController.swift
//  EventsApp
//
//  Created by Adrian Zuparic on 23/12/20.
//

import UIKit

final class EventDetailViewController: UIViewController {
    struct Event: Codable {
        let name: String
        let date: Date
        let location: String
    }
    
    let timeRemainingStackView: TimeRemainingStackView = {
        let stackView = TimeRemainingStackView()
        stackView.setup()
        return stackView
    }()

    let backgroundImageView = UIImageView()
    var viewModel: EventDetailViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        viewModel.onUpdate = { [weak self] in
            guard let self = self, let timeRemainingViewModel = self.viewModel.timeRemainingViewModel else { return }
            self.backgroundImageView.image = self.viewModel.image
            self.timeRemainingStackView.update(with: timeRemainingViewModel)
        }
        
        viewModel.viewDidLoad()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel.viewDidDissapear()
    }
}

// MARK: - Configure view
private extension EventDetailViewController {
    func configureView() {
        view.addSubview(backgroundImageView)
        view.addSubview(timeRemainingStackView)
        
        let editButton = UIBarButtonItem(image: UIImage(systemName: "pencil"), style: .plain, target: viewModel, action: #selector(viewModel.editButtonTapped))
        let shareButton = UIBarButtonItem(image: UIImage(systemName: "square.and.arrow.up"), style: .plain, target: self, action: #selector(shareButtonTapped))

        navigationItem.rightBarButtonItems = [editButton, shareButton]
        configureTextColor()
        configureConstraints()
    }
    
    func configureConstraints() {
        backgroundImageView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        timeRemainingStackView.snp.makeConstraints { make in
            make.centerX.centerY.equalTo(backgroundImageView)
        }
    }
    
    func configureTextColor() {
        let point = CGPoint(x: timeRemainingStackView.center.x, y: timeRemainingStackView.center.y)
        if let pixelBuffer = backgroundImageView.image?.pixelBuffer(), let color = pixelBuffer.color(at: point) {
            let colorSpaceModel = color.cgColor.colorSpace?.model
            let colorComponents = color.cgColor.components

            if colorSpaceModel == .rgb, let r = colorComponents?[0], let g = colorComponents?[1], let b = colorComponents?[2] {
                let brightness = (0.299 * r + 0.587 * g + 0.114 * b) * 255
                if brightness > 128 {
                    timeRemainingStackView.timeRemainingLabels.forEach({ $0.textColor = .black})
                } else {
                    timeRemainingStackView.timeRemainingLabels.forEach({ $0.textColor = .white})
                }
            } else {
                timeRemainingStackView.timeRemainingLabels.forEach({ $0.textColor = .white})
            }
        }
    }
}

// MARK: - Actions
private extension EventDetailViewController {
    @objc func shareButtonTapped() {
        guard let screenshot = takeScreenshot() else { return }
        let activityViewController = UIActivityViewController(activityItems: [screenshot], applicationActivities: nil)
        present(activityViewController, animated: true)
    }
    
    func takeScreenshot() -> UIImage? {
        let renderer = UIGraphicsImageRenderer(bounds: view.bounds)
        return renderer.image { _ in
            view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        }
    }
}
