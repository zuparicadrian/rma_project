//
//  EditEventViewController.swift
//  EventsApp
//
//  Created by Adrian Zuparic on 28/12/20.
//

import UIKit

final class EditEventViewController: UIViewController {
    var viewModel: EditEventViewModel!
    var tableView: UITableView = {
        let tableView = UITableView()
        tableView.register(TitleSubtitleCell.self, forCellReuseIdentifier: TitleSubtitleCell.identifier)
        return tableView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        viewModel.onUpdate = { [weak self] in
            self?.tableView.reloadData()
        }
        viewModel.viewDidLoad()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel.viewDidDisappear()
    }

    @objc private func tappedDone() {
        viewModel.tappedDone()
    }
}

// MARK: - Configure view
private extension EditEventViewController {
    func configureView() {
        navigationItem.title = viewModel.title
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(tappedDone))

        tableView.dataSource = self
        tableView.delegate = self
        view.addSubview(tableView)
        configureConstraints()
    }
    
    func configureConstraints() {
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

// MARK: - TableView
extension EditEventViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.numberOfRows()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellViewModel = viewModel.cell(for: indexPath)
        switch cellViewModel {
        case .titleSubtitle(let titleSubtitleCellViewModel):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TitleSubtitleCell.identifier, for: indexPath) as? TitleSubtitleCell else { return UITableViewCell() }
            cell.update(with: titleSubtitleCellViewModel)
            cell.subtitleTextField.delegate = self
            cell.selectionStyle = .none
            return cell
        }
    }
}

extension EditEventViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectRow(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension EditEventViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let currentText = textField.text else { return false }
        let text = currentText + string
        let point = textField.convert(textField.bounds.origin, to: tableView)
        if let indexPath = tableView.indexPathForRow(at: point) {
            viewModel.updateCell(indexPath: indexPath, subtitle: text)
        }
        return true
    }
}


